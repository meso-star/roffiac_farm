/* Copyright (C) 2022-2023 EDStar (http://www.edstar.cnrs.fr/prod/fr/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

/* ground dimensions */
#define GROUND_DEPTH 3
#define GROUND_WIDTH 100
#define GROUND_LENGHT 100

/* farm dimensions */ 
#define FARM_WIDTH 8
#define FARM_LENGHT 20
#define FARM_HEIGHT1 2.9
#define FARM_HEIGHT2 1.5

/* wall dimensions */
#define WALL_THICKNESS_INT 0.1 
#define WALL_THICKNESS_MID 0.3 
#define WALL_THICKNESS_EXT 0.1 

/* first floor dimensions  */
#define FLOOR1_INSULATION_THICKNESS 0.1
#define FLOOR1_THICKNESS 0.1

/* second floor dimensions */
#define FLOOR2_THICKNESS 0.1
#define DECKING_THICKNESS 0.1
#define FLOOR2_OVERHANG 0

/* roof dimensions */
#define ROOF_LAYER1_THICKNESS 0.3
#define ROOF_LAYER2_THICKNESS 0.05
#define ROOF_ANGLE 40
#define ROOF_OVERHANG 1 

/* thermometer probe dimensions */
#define PROBE_X 3.5
#define PROBE_Y 0
#define PROBE_Z 1 
#define PROBE_SIZE 0.1

/* heater dimensions */
#define HEATER_X 3.7
#define HEATER_Y 0
#define HEATER_DX 0.2
#define HEATER_DY 10 
#define HEATER_DZ 1.5
