/* Copyright (C) 2022-2023 EDStar (http://www.edstar.cnrs.fr/prod/fr/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <star/scad.h>
#include <rsys/rsys.h>
#include <rsys/math.h>
#include <rsys/stretchy_array.h>

#include "data.h"
#define ERR(Expr) if((res = (Expr)) != RES_OK) goto error; else (void)0

struct farm {
  double ground_depth;
  double ground_width;
  double ground_lenght;
  double farm_width; 
  double farm_lenght; 
  double farm_height1;
  double farm_height2;
  double wall_thickness_int;
  double wall_thickness_mid;
  double wall_thickness_ext;
  double floor1_insulation_thickness;
  double floor1_thickness;
  double floor2_thickness;
  double floor2_overhang;
  double decking_thickness;
  double roof_layer1_thickness;
  double roof_layer2_thickness;
  double roof_angle;
  double roof_overhang;
  double probe_position[3];
  double probe_size;
  double heater_position[2];
  double heater_size[3];
};

struct medium {
  struct scad_geometry* ground;
  struct scad_geometry* wall_int;
  struct scad_geometry* wall_mid;
  struct scad_geometry* wall_ext;
  struct scad_geometry* floor1_insulation;
  struct scad_geometry* floor1;
  struct scad_geometry* floor2;
  struct scad_geometry* decking;
  struct scad_geometry* roof_layer1;
  struct scad_geometry* roof_layer2;
  struct scad_geometry* probe;
  struct scad_geometry* heater;
  struct scad_geometry* cavity1;
  struct scad_geometry* cavity2;
};

struct point {
  double x;
  double y;
};

static res_T
init_farm(struct farm* farm)
{
  res_T res = RES_OK;

  if (!farm) {
    res = RES_BAD_ARG;
    goto error;
  }
  
  farm->ground_depth = GROUND_DEPTH;
  farm->ground_width = GROUND_WIDTH;
  farm->ground_lenght = GROUND_LENGHT;
  farm->farm_width = FARM_WIDTH;
  farm->farm_lenght = FARM_LENGHT;
  farm->farm_height1 = FARM_HEIGHT1;
  farm->farm_height2 = FARM_HEIGHT2;
  farm->wall_thickness_int = WALL_THICKNESS_INT;
  farm->wall_thickness_mid = WALL_THICKNESS_MID;
  farm->wall_thickness_ext = WALL_THICKNESS_EXT;
  farm->floor1_insulation_thickness = FLOOR1_INSULATION_THICKNESS;
  farm->floor1_thickness = FLOOR1_THICKNESS;
  farm->floor2_thickness = FLOOR2_THICKNESS;
  farm->floor2_overhang = FLOOR2_OVERHANG;
  farm->decking_thickness = DECKING_THICKNESS;
  farm->roof_layer1_thickness = ROOF_LAYER1_THICKNESS;
  farm->roof_layer2_thickness = ROOF_LAYER2_THICKNESS;
  farm->roof_angle = ROOF_ANGLE;
  farm->roof_overhang = ROOF_OVERHANG;
  farm->probe_position[0] = PROBE_X;
  farm->probe_position[1] = PROBE_Y;
  farm->probe_position[2] = PROBE_Z;
  farm->probe_size = PROBE_SIZE;
  farm->heater_position[0] = HEATER_X;
  farm->heater_position[1] = HEATER_Y;
  farm->heater_size[0] = HEATER_DX;
  farm->heater_size[1] = HEATER_DY;
  farm->heater_size[2] = HEATER_DZ;

exit:
  return res;
error:
  goto exit;
}

static res_T
build_ground(const struct farm* farm, struct scad_geometry** ground)
{
  res_T res = RES_OK;
  double p[3];
  double d[3];
  
  if (!farm || !ground) {
    res = RES_BAD_ARG;
    goto error;
  }

  p[0] = - farm->ground_width / 2;  
  p[1] = - farm->ground_lenght / 2;  
  p[2] = - farm->ground_depth;  

  d[0] = farm->ground_width;
  d[1] = farm->ground_lenght;
  d[2] = farm->ground_depth;
  
  ERR(scad_add_box("S_ground", p, d, ground));

exit:
  return res;
error:
  goto exit;
}

static void get_position
  (const size_t ivert, double pos[2], void* ctx)
{
  struct point* points = (struct point*)ctx;

  pos[0] = points[ivert].x;
  pos[1] = points[ivert].y;
}


static res_T
build_gable
  (const double p[3],
   const double base,
   const double height,
   const double thickness,
   struct scad_geometry** gable)
{
  res_T res = RES_OK;
  struct point list_points[3];
  double z;
  double ex[3] = {1, 0, 0};
  double ey[3] = {0, 1, 0};
  struct scad_geometry* triangle;
  
  list_points[0].x = p[0] - base/2;
  list_points[0].y = p[1];

  list_points[1].x = p[0] + base/2;
  list_points[1].y = p[1];
    
  list_points[2].x = p[0];
  list_points[2].y = p[1] + height;
   
  z = p[2];

  /*ERR(scad_add_polygon(NULL, x, y, z, 3, &triangle));*/
  ERR(scad_add_polygon(NULL, get_position, &list_points, z, 3, &triangle));


  ERR(scad_geometry_rotate(triangle, p, ex, PI/2));
  
  ey[1] = thickness;
  ERR(scad_geometry_extrude(triangle, NULL, ey, gable));

  ERR(scad_geometry_delete(triangle));
  
exit:
  return res;
error:
  goto exit;
}


static res_T
build_walls
  (const struct farm* farm,
   struct scad_geometry* slab,
   struct scad_geometry** wall_int,
   struct scad_geometry** wall_mid,
   struct scad_geometry** wall_ext)
{
  res_T res = RES_OK;
  double p1[3];
  double d1[3];
  double p2[3];
  double d2[3];
  double p3[3];
  double dir[3];
  double height;
  double base;
  struct scad_geometry* box1;
  struct scad_geometry* box2;
  struct scad_geometry* wall1;
  struct scad_geometry* wall2;
  struct scad_geometry* gable1;
  struct scad_geometry* gable2;
  struct scad_geometry* list[2];
  
  if (!farm || !wall_int || !wall_mid || !wall_ext) {
    res = RES_BAD_ARG;
    goto error;
  }

/* internal wall */
  p1[0] = - farm->farm_width / 2;  
  p1[1] = - farm->farm_lenght / 2;  
  p1[2] = 0;

  d1[0] = farm->farm_width;
  d1[1] = farm->farm_lenght;
  d1[2] = farm->farm_height1 + farm->farm_height2;

  p2[0] = p1[0] - farm->wall_thickness_int;  
  p2[1] = p1[1] - farm->wall_thickness_int;   
  p2[2] = p1[2];

  d2[0] = d1[0] + 2*farm->wall_thickness_int;
  d2[1] = d1[1] + 2*farm->wall_thickness_int; 
  d2[2] = d1[2];
  
  ERR(scad_add_box(NULL, p1, d1, &box1));
  ERR(scad_add_box(NULL, p2, d2, &box2));
  ERR(scad_cut_geometries
      (NULL,
       &box2, 1,
       &box1, 1,
       &wall1));

  ERR(scad_geometry_delete(box1));
  ERR(scad_geometry_delete(box2));

  /*gable 1*/
  p3[0] = 0;
  p3[1] = -farm->farm_lenght / 2;
  p3[2] = farm->farm_height1 + farm->farm_height2;
  /*p3[2] = farm->wall_height;*/
  base = farm->farm_width 
    + 2*(farm->wall_thickness_int 
        + farm->wall_thickness_mid
        + farm->wall_thickness_ext);
  height = (farm->farm_width / 2 
      + farm->wall_thickness_int 
      + farm->wall_thickness_mid 
      + farm->wall_thickness_ext)*tan(MDEG2RAD(farm->roof_angle)); 
 
  ERR(build_gable(p3, base, height, -farm->wall_thickness_int, &gable1));

   /*gable 2*/
  ERR(scad_geometry_copy(gable1, NULL, &gable2));
  dir[0] = 0;
  dir[1] = farm->farm_lenght + farm->wall_thickness_int;
  dir[2] = 0;
  ERR(scad_geometry_translate(gable2, dir));
 
  list[0] = gable1;
  list[1] = gable2;
  ERR(scad_fuse_geometries(NULL, &wall1, 1, list, 2, &wall2));
  ERR(scad_cut_geometries
      ("S_wall_int",
       &wall2, 1,
       &slab, 1,
       wall_int));
  ERR(scad_geometry_delete(gable1));
  ERR(scad_geometry_delete(gable2));
  ERR(scad_geometry_delete(wall1));
  ERR(scad_geometry_delete(wall2));


/* mid wall */
  p1[0] -= farm->wall_thickness_int;
  p1[1] -= farm->wall_thickness_int; 

  d1[0] += 2*farm->wall_thickness_int;
  d1[1] += 2*farm->wall_thickness_int;

  p2[0] = p1[0] - farm->wall_thickness_mid;  
  p2[1] = p1[1] - farm->wall_thickness_mid;   

  d2[0] = d1[0] + 2*farm->wall_thickness_mid;
  d2[1] = d1[1] + 2*farm->wall_thickness_mid; 
 
  ERR(scad_add_box(NULL, p1, d1, &box1));
  ERR(scad_add_box(NULL, p2, d2, &box2));
  ERR(scad_cut_geometries
      (NULL,
       &box2, 1,
       &box1, 1,
       &wall1));

  ERR(scad_geometry_delete(box1));
  ERR(scad_geometry_delete(box2));

  /*gable 1*/
  p3[0] = 0;
  p3[1] = -farm->farm_lenght / 2 -farm->wall_thickness_int;
  p3[2] = farm->farm_height1 + farm->farm_height2;
  /*p3[2] = farm->wall_height;*/
  base = farm->farm_width 
    + 2*(farm->wall_thickness_int 
        + farm->wall_thickness_mid
        + farm->wall_thickness_ext);
  height = (farm->farm_width / 2 
      + farm->wall_thickness_int 
      + farm->wall_thickness_mid 
      + farm->wall_thickness_ext)*tan(MDEG2RAD(farm->roof_angle)); 
 
  ERR(build_gable(p3, base, height, -farm->wall_thickness_mid, &gable1));

   /*gable 2*/
  ERR(scad_geometry_copy(gable1, NULL, &gable2));
  dir[0] = 0;
  dir[1] = farm->farm_lenght + 2*farm->wall_thickness_int 
    + farm->wall_thickness_mid;
  dir[2] = 0;
  ERR(scad_geometry_translate(gable2, dir));
 
  list[0] = gable1;
  list[1] = gable2;
  ERR(scad_fuse_geometries(NULL, &wall1, 1, list, 2, &wall2));
  ERR(scad_cut_geometries
      ("S_wall_mid",
       &wall2, 1,
       &slab, 1,
       wall_mid));
  ERR(scad_geometry_delete(gable1));
  ERR(scad_geometry_delete(gable2));
  ERR(scad_geometry_delete(wall1));
  ERR(scad_geometry_delete(wall2));

/* external wall */
  p1[0] -= farm->wall_thickness_mid;
  p1[1] -= farm->wall_thickness_mid; 

  d1[0] += 2*farm->wall_thickness_mid;
  d1[1] += 2*farm->wall_thickness_mid;

  p2[0] = p1[0] - farm->wall_thickness_ext;  
  p2[1] = p1[1] - farm->wall_thickness_ext;   

  d2[0] = d1[0] + 2*farm->wall_thickness_ext;
  d2[1] = d1[1] + 2*farm->wall_thickness_ext; 
 
  ERR(scad_add_box(NULL, p1, d1, &box1));
  ERR(scad_add_box(NULL, p2, d2, &box2));
  ERR(scad_cut_geometries
      (NULL,
       &box2, 1,
       &box1, 1,
       &wall1));

  ERR(scad_geometry_delete(box1));
  ERR(scad_geometry_delete(box2));

  /*gable 1*/
  p3[0] = 0;
  p3[1] = - farm->farm_lenght / 2 
    - farm->wall_thickness_int
    - farm->wall_thickness_mid;
  /*p3[2] = farm->wall_height;*/
  p3[2] = farm->farm_height1 + farm->farm_height2;
  base = farm->farm_width 
    + 2*(farm->wall_thickness_int 
        + farm->wall_thickness_mid
        + farm->wall_thickness_ext);
  height = (farm->farm_width / 2 
      + farm->wall_thickness_int 
      + farm->wall_thickness_mid 
      + farm->wall_thickness_ext)*tan(MDEG2RAD(farm->roof_angle)); 
 
  ERR(build_gable(p3, base, height, -farm->wall_thickness_ext, &gable1));

   /*gable 2*/
  ERR(scad_geometry_copy(gable1, NULL, &gable2));
  dir[0] = 0;
  dir[1] = farm->farm_lenght + 2*farm->wall_thickness_int 
    + 2*farm->wall_thickness_mid + farm->wall_thickness_ext;
  dir[2] = 0;
  ERR(scad_geometry_translate(gable2, dir));
 
  list[0] = gable1;
  list[1] = gable2;
  ERR(scad_fuse_geometries(NULL, &wall1, 1, list, 2, &wall2));
  ERR(scad_cut_geometries
      ("S_wall_ext",
       &wall2, 1,
       &slab, 1,
       wall_ext));
  ERR(scad_geometry_delete(gable1));
  ERR(scad_geometry_delete(gable2));
  ERR(scad_geometry_delete(wall1));
  ERR(scad_geometry_delete(wall2));

exit:
  return res;
error:
  goto exit;
}

static res_T
build_floor1
  (const struct farm* farm,
   struct scad_geometry** insulation,
   struct scad_geometry** slab)
{
  res_T res = RES_OK;
  double p[3];
  double d[3];
  
  if (!farm || !slab || !insulation) {
    res = RES_BAD_ARG;
    goto error;
  }

  /*insulation*/
  p[0] = - farm->farm_width / 2;  
  p[1] = - farm->farm_lenght / 2;  
  p[2] = 0;  

  d[0] = farm->farm_width;
  d[1] = farm->farm_lenght;
  d[2] = farm->floor1_insulation_thickness;
  
  ERR(scad_add_box("S_floor1_insulation", p, d, insulation));

  /*slab*/
  p[2] += farm->floor1_insulation_thickness;  
  d[2] = farm->floor1_thickness;

  ERR(scad_add_box("S_floor1", p, d, slab));

exit:
  return res;
error:
  goto exit;
}

static res_T
build_floor2
  (const struct farm* farm,
   struct scad_geometry** slab,
   struct scad_geometry** decking)
{
  res_T res = RES_OK;
  double p[3];
  double d[3];
  
  if (!farm || !slab || !decking) {
    res = RES_BAD_ARG;
    goto error;
  }

  /*slab*/
  p[0] = - farm->farm_width / 2 - farm->floor2_overhang;  
  p[1] = - farm->farm_lenght / 2 - farm->floor2_overhang;  
  p[2] = farm->farm_height1;  

  d[0] = farm->farm_width + 2*farm->floor2_overhang;
  d[1] = farm->farm_lenght + 2*farm->floor2_overhang;  
  d[2] = farm->floor2_thickness;
  
  ERR(scad_add_box("S_floor2", p, d, slab));

  /*decking*/
  p[0] = - farm->farm_width / 2;  
  p[1] = - farm->farm_lenght / 2;  
  p[2] += farm->floor2_thickness;  

  d[0] = farm->farm_width;
  d[1] = farm->farm_lenght;  
  d[2] = farm->decking_thickness;

  ERR(scad_add_box("S_decking", p, d, decking));

exit:
  return res;
error:
  goto exit;
}

static res_T
build_roof_layer
  (const struct farm* farm,
   const double height,
   const double thickness,
   struct scad_geometry** layer,
   double* dh)
{
  res_T res = RES_OK;
  double p[3];
  double d[2];
  double pt[3];
  double ey[3] = {0, 1, 0} ;
  double ez[3] = {0, 0, 1} ;
  double angle;
  double gable_half_width;
  double panel_lenght;
  double half_lenght;
  struct scad_geometry* panel_face1 = NULL;
  struct scad_geometry* panel_face2 = NULL;
  struct scad_geometry* panel1 = NULL;
  struct scad_geometry* panel2 = NULL;
  
  if (!farm || !layer || height<=0 || thickness<=0) {
    res = RES_BAD_ARG;
    goto error;
  }

  angle = MDEG2RAD(farm->roof_angle);
  gable_half_width =
    farm->farm_width / 2 
    + farm->wall_thickness_int 
    + farm->wall_thickness_mid 
    + farm->wall_thickness_ext; 
  
  half_lenght = 
    farm->farm_lenght/2  
    + farm->wall_thickness_int 
    + farm->wall_thickness_mid 
    + farm->wall_thickness_ext; 
  
  panel_lenght = gable_half_width / cos(angle) + farm->roof_overhang;

  *dh = thickness / cos(angle);

  /* pan 1 layer 1 */
  p[0] = -gable_half_width - farm->roof_overhang;
  p[1] = -half_lenght - farm->roof_overhang; 
  p[2] = height;
  
  d[0] = panel_lenght;
  d[1] = 2*half_lenght + 2*farm->roof_overhang;

  ERR(scad_add_rectangle(NULL, p, d, &panel_face1));

  pt[0] = -gable_half_width;
  pt[1] = 0;
  pt[2] = height;
  
  ERR(scad_geometry_rotate(panel_face1, pt, ey, -angle));
  
  ez[2] = thickness / cos(angle);
  ERR(scad_geometry_extrude(panel_face1, NULL, ez, &panel1));

 /* pan 2 layer 1 */
  p[0] = gable_half_width + farm->roof_overhang;
  p[1] = -half_lenght - farm->roof_overhang; 
  p[2] = height;
  
  d[0] = -panel_lenght;
  d[1] = 2*half_lenght + 2*farm->roof_overhang;

  ERR(scad_add_rectangle(NULL, p, d, &panel_face2));

  pt[0] = gable_half_width;
  pt[1] = 0;
  pt[2] = height;
  
  ERR(scad_geometry_rotate(panel_face2, pt, ey, angle));
  
  ez[2] = thickness / cos(angle);
  ERR(scad_geometry_extrude(panel_face2, NULL, ez, &panel2));

  ERR(scad_fuse_geometries(NULL, 
        &panel1, 1,
        &panel2, 1,
        layer));

  ERR(scad_geometry_delete(panel_face1));
  ERR(scad_geometry_delete(panel_face2));
  ERR(scad_geometry_delete(panel1));
  ERR(scad_geometry_delete(panel2));

exit:
  return res;
error:
  goto exit;
}

static res_T
build_roof
  (const struct farm* farm,
   struct scad_geometry** layer1,
   struct scad_geometry** layer2)
{
  res_T res = RES_OK;
  double height;
  double thickness;
  double dh;

  height = farm->farm_height1 + farm->farm_height2; 
  thickness = farm->roof_layer1_thickness;
  ERR(build_roof_layer(farm, height, thickness, layer1, &dh));
  ERR(scad_geometry_rename(*layer1, "S_roof_layer1"));

  height += dh; 
  thickness = farm->roof_layer2_thickness;
  ERR(build_roof_layer(farm, height, thickness, layer2, &dh));
  ERR(scad_geometry_rename(*layer2, "S_roof_layer2"));

exit:
  return res;
error:
  goto exit;
}

static res_T
build_cavity
  (const struct farm* farm,
   struct scad_geometry* probe,
   struct scad_geometry* heater,
   struct scad_geometry** cavity1,
   struct scad_geometry** cavity2)
{
  res_T res = RES_OK;
  double p[3];
  double d[3];
  double base;
  double height;
  struct scad_geometry* etage;
  struct scad_geometry* comble;
  struct scad_geometry* list[2];
  
  if (!farm || !probe || !heater || !cavity1 || !cavity2) {
    res = RES_BAD_ARG;
    goto error;
  }

  list[0] = probe;
  list[1] = heater;

  /* level 1 */
  p[0] = - farm->farm_width / 2;  
  p[1] = - farm->farm_lenght / 2;  
  p[2] = farm->floor1_thickness + farm->floor1_insulation_thickness;  

  d[0] = farm->farm_width;
  d[1] = farm->farm_lenght;
  d[2] = farm->farm_height1 - p[2];
  
  ERR(scad_add_box(NULL, p, d, cavity1));

  ERR(scad_cut_geometries
      ("F_cavity1",
       cavity1, 1,
       list, 2,
       cavity1));

  /* level 2 */
  p[0] = - farm->farm_width / 2;  
  p[1] = - farm->farm_lenght / 2;  
  p[2] = farm->farm_height1 + farm->floor2_thickness + farm->decking_thickness;  

  d[0] = farm->farm_width;
  d[1] = farm->farm_lenght;
  d[2] = farm->farm_height1 + farm->farm_height2 - p[2];
  
  ERR(scad_add_box(NULL, p, d, &etage));

  p[0] = 0;
  p[1] = - farm->farm_lenght / 2;  
  p[2] = farm->farm_height1 + farm->farm_height2;
  base = farm->farm_width 
    + 2*(farm->wall_thickness_int 
        + farm->wall_thickness_mid
        + farm->wall_thickness_ext);
  height = (farm->farm_width / 2 
      + farm->wall_thickness_int 
      + farm->wall_thickness_mid 
      + farm->wall_thickness_ext)*tan(MDEG2RAD(farm->roof_angle)); 
  ERR(build_gable(p, base, height, farm->farm_lenght, &comble));

  ERR(scad_fuse_geometries(NULL, &etage, 1, &comble, 1, cavity2));
  
  ERR(scad_geometry_delete(etage));
  ERR(scad_geometry_delete(comble));

  ERR(scad_cut_geometries
      ("F_cavity2",
       cavity2, 1,
       list, 2,
       cavity2));

exit:
  return res;
error:
  goto exit;
}

static res_T
build_probe(const struct farm* farm, struct scad_geometry** probe)
{
  res_T res = RES_OK;
  double p[3];
  double d[3];
  
  if (!farm || !probe) {
    res = RES_BAD_ARG;
    goto error;
  }

  d[0] = farm->probe_size;
  d[1] = farm->probe_size;
  d[2] = farm->probe_size;

  p[0] = farm->probe_position[0] - d[0]/2;
  p[1] = farm->probe_position[1] - d[1]/2;
  p[2] = farm->probe_position[2] - d[2]/2;

  ERR(scad_add_box("S_probe", p, d, probe)); 

exit:
  return res;
error:
  goto exit;
}

static res_T
build_heater(const struct farm* farm, struct scad_geometry** heater)
{
  res_T res = RES_OK;
  double p[3];
  double d[3];
  
  if (!farm || !heater) {
    res = RES_BAD_ARG;
    goto error;
  }

  d[0] = farm->heater_size[0];
  d[1] = farm->heater_size[1];
  d[2] = farm->heater_size[2];

  p[0] = farm->heater_position[0] - d[0]/2;
  p[1] = farm->heater_position[1] - d[1]/2;
  p[2] = farm->floor1_thickness + farm->floor1_insulation_thickness + 0.1;

  ERR(scad_add_box("S_heater", p, d, heater)); 

exit:
  return res;
error:
  goto exit;
}

static res_T
build_bc
  (struct medium* medium,
   struct scad_geometry* geom,
   char* name,
   struct scad_geometry*** bc)
{
  res_T res = RES_OK;
  size_t count = 0;
  struct scad_geometry* bound;
  struct scad_geometry* list[14];

  if (!medium || !geom || !bc) {
    res = RES_BAD_ARG;
    goto error;
  }

  list[0] = medium->ground;
  list[1] = medium->wall_int;
  list[2] = medium->wall_mid;
  list[3] = medium->wall_ext;
  list[5] = medium->floor1_insulation;
  list[4] = medium->floor1;
  list[6] = medium->floor2;
  list[7] = medium->decking;
  list[8] = medium->roof_layer1;
  list[9] = medium->roof_layer2;
  list[10] = medium->probe;
  list[11] = medium->heater;
  list[12] = medium->cavity1;
  list[13] = medium->cavity2;

  ERR(scad_geometries_common_boundaries(name, list, 14, &geom, 1, &bound));

  ERR(scad_geometry_get_count(bound, &count));
  if (count>0) {
    sa_push(*bc, bound);
  } else {
    ERR(scad_geometry_delete(bound));
  }
    
exit:
  return res;
error:
  goto exit;
}

static res_T
build_connection
  (struct scad_geometry* medium1,
   struct scad_geometry* medium2,
   char* name,
   struct scad_geometry*** connection)
{
  res_T res = RES_OK;
  size_t count = 0;
  struct scad_geometry* connect;

  if (!medium1 || !medium2 || !connection) {
    res = RES_BAD_ARG;
    goto error;
  }

  ERR(scad_geometries_common_boundaries 
      (name,
       &medium1, 1,
       &medium2, 1,
       &connect));
  
  ERR(scad_geometry_get_count(connect, &count));
  if (count>0) {
    sa_push(*connection, connect);
  } else {
    ERR(scad_geometry_delete(connect));
  }

exit:
  return res;
error:
  goto exit;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

int 
main(int argc, char** argv)
{
  res_T res = RES_OK;
  size_t i;
  struct farm farm;
  struct medium medium;
  struct scad_geometry** bc = NULL; /* list of boundary faces */
  struct scad_geometry** connection = NULL; /* list of solid/fluid connection faces */
 
  (void)argc; (void)argv;

  ERR(scad_initialize(NULL, NULL, 1));

  /* fetch data */
  ERR(init_farm(&farm));

  /* build solid mediums */
  ERR(build_ground(&farm, &medium.ground));
  ERR(build_floor1(&farm, &medium.floor1_insulation, &medium.floor1));
  ERR(build_floor2(&farm, &medium.floor2, &medium.decking));
  ERR(build_walls(&farm, medium.floor2,
        &medium.wall_int, &medium.wall_mid, &medium.wall_ext));
  ERR(build_roof(&farm, &medium.roof_layer1, &medium.roof_layer2));
  ERR(build_probe(&farm, &medium.probe));
  ERR(build_heater(&farm, &medium.heater));

  /* build fluid mediums*/
  ERR(build_cavity(&farm, medium.probe, medium.heater,
        &medium.cavity1, &medium.cavity2));

  /* scene partitionning */
  ERR(scad_scene_partition());

  /* build boundary faces */
  ERR(build_bc(&medium, medium.ground, "B_ground", &bc));
  ERR(build_bc(&medium, medium.wall_ext, "B_wall", &bc));
  ERR(build_bc(&medium, medium.roof_layer1, "B_roof_layer1", &bc));
  ERR(build_bc(&medium, medium.roof_layer2, "B_roof_layer2", &bc));
  ERR(build_bc(&medium, medium.floor2, "B_floor2", &bc));

  /* build solid/fluid connection faces */
  ERR(build_connection(medium.cavity1,
        medium.wall_int,
        "C_cavity1_wall_int", 
        &connection));

  ERR(build_connection(medium.cavity1,
        medium.floor1,
        "C_cavity1_floor1", 
        &connection));

  ERR(build_connection(medium.cavity1,
        medium.floor2,
        "C_cavity1_floor2", 
        &connection));

  ERR(build_connection(medium.cavity1,
        medium.probe,
        "C_cavity1_probe", 
        &connection));

  ERR(build_connection(medium.cavity1,
        medium.heater,
        "C_cavity1_heater", 
        &connection));
 
  ERR(build_connection(medium.cavity2,
        medium.decking,
        "C_cavity2_decking", 
        &connection));

  ERR(build_connection(medium.cavity2,
        medium.wall_int,
        "C_cavity2_wall_int", 
        &connection));

  ERR(build_connection(medium.cavity2,
        medium.wall_mid,
        "C_cavity2_wall_mid", 
        &connection));

  ERR(build_connection(medium.cavity2,
        medium.wall_ext,
        "C_cavity2_wall_ext", 
        &connection));

  ERR(build_connection(medium.cavity2,
        medium.roof_layer1,
        "C_cavity2_roof_layer1", 
        &connection));

  ERR(build_connection(medium.cavity2,
        medium.probe,
        "C_cavity2_probe", 
        &connection));

  ERR(build_connection(medium.cavity2,
        medium.heater,
        "C_cavity2_heater", 
        &connection));

  /* scene meshing */
  ERR(scad_scene_mesh());

  /* stl file export */
  ERR(scad_stl_export(medium.ground, NULL, 0, 0));
  ERR(scad_stl_export(medium.wall_int, NULL, 0, 0));
  ERR(scad_stl_export(medium.wall_mid, NULL, 0, 0));
  ERR(scad_stl_export(medium.wall_ext, NULL, 0, 0));
  ERR(scad_stl_export(medium.floor1_insulation, NULL, 0, 0));
  ERR(scad_stl_export(medium.floor1, NULL, 0, 0));
  ERR(scad_stl_export(medium.floor2, NULL, 0, 0));
  ERR(scad_stl_export(medium.decking, NULL, 0, 0));
  ERR(scad_stl_export(medium.roof_layer1, NULL, 0, 0));
  ERR(scad_stl_export(medium.roof_layer2, NULL, 0, 0));
  ERR(scad_stl_export(medium.probe, NULL, 0, 0));
  ERR(scad_stl_export(medium.heater, NULL, 0, 0));
  ERR(scad_stl_export(medium.cavity1, NULL, 0, 0));
  ERR(scad_stl_export(medium.cavity2, NULL, 0, 0));

  for (i=0; i<sa_size(bc); ++i){
    ERR(scad_stl_export(bc[i], NULL, 0, 0));
    ERR(scad_stl_export_split(bc[i], NULL, 0));
  }

  for (i=0; i<sa_size(connection); ++i){
    ERR(scad_stl_export(connection[i], NULL, 0, 0));
  }

exit:
  scad_geometry_delete(medium.ground);
  scad_geometry_delete(medium.wall_int);
  scad_geometry_delete(medium.wall_mid);
  scad_geometry_delete(medium.wall_ext);
  scad_geometry_delete(medium.floor1_insulation);
  scad_geometry_delete(medium.floor1);
  scad_geometry_delete(medium.floor2);
  scad_geometry_delete(medium.decking);
  scad_geometry_delete(medium.roof_layer1);
  scad_geometry_delete(medium.roof_layer2);
  scad_geometry_delete(medium.probe);
  scad_geometry_delete(medium.heater);
  scad_geometry_delete(medium.cavity1);
  scad_geometry_delete(medium.cavity2);
  for (i=0; i<sa_size(bc); ++i){
    scad_geometry_delete(bc[i]);
  }
  for (i=0; i<sa_size(connection); ++i){
    scad_geometry_delete(connection[i]);
  }
  if (bc) sa_release(bc);
  if (connection) sa_release(connection);
  scad_finalize();
  return res;
error:
  goto exit;
}
