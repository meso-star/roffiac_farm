# The Roffiac farm generator 

The Roffiac farm generator is a program for creating geometric content based on
the Star-CAD library. It allows you to create a CAD model of a typical farm
found in central France. The geometrical parameters are to be filled in the
file `data.h`.

As output, the program generates a set of stl files describing: 

- the solid mediums prefixed by `S_` ; 
- the fluid mediums prefixed by `F_` ; 
- the boundary faces prefixed by `B_` ; 
- the connections between the internal cavities and the solids prefixed by `C_`. 

These stl files constitute a conformal mesh of the scene that can be used as
input data for a program like [Stardis](https://gitlab.com/meso-star/stardis).

## How to build

This program relies on [CMake](http://www.cmake.org),
[RCMake](https://gitlab.com/vaplv/rcmake/#tab-readme) and the
[Rsys](https://gitlab.com/vaplv/rsys/#tab-readme) package to build. It also
depends on the [Star-CAD](https://gitlab.com/meso-star/star-cad/) library.

You can first install Star-CAD and its dependencies via the star-cad branch of
[star-engine](https://gitlab.com/meso-star/star-engine/-/tree/star-cad) by
following these
[instructions](https://gitlab.com/meso-star/star-engine/-/tree/star-cad).

We then assume that the star-engine is installed in the ~/star-engine/
directory. Then you can clone this repository and build the project:

    ~ $ git clone https://gitlab.com/meso-star/roffiac_farm.git
    ~ $ mkdir roffiac_farm/build && cd roffiac/build
    ~/roffiac_farm/build $ cmake ../cmake -DCMAKE_PREFIX_PATH=~/star-engine/local 
    ~/roffiac_farm/build $ make 
    ~/roffiac_farm/build $ source ~/star-engine/local/etc/star-engine.profile
 
And to execute the program:

    ~/roffiac_farm/build $ ./farm_generator

## License

Copyright (C) 2022-2023 EDStar (http://www.edstar.cnrs.fr/prod/fr/)
The Roffiac farm generator is free software released under the GPL v3+ license:
GNU GPL version 3 or later. You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
